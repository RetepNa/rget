pub mod context;
use lazy_static::lazy_static;
use regex::{Regex, RegexBuilder};
use std::{
    fs::File,
    io::{self, BufRead},
    path::Path,
};
use base64::encode;

lazy_static! {
    static ref REGEX_ILLEGAL_CHARACTERS: Regex = Regex::new(r#"[/\?<>\\:\*\|":]"#).unwrap();
    static ref REGEX_WINDOWS_RESERVED: Regex =
        RegexBuilder::new(r#"(?i)^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$"#)
            .case_insensitive(true)
            .build()
            .unwrap();
    static ref REGEX_RESERVED: Regex = Regex::new(r#"^\.+$"#).unwrap();
    static ref REGEX_WINDOWS_TRAILING: Regex = Regex::new(r#"^\.+$"#).unwrap();
    static ref REGEX_CONTROL_CHARACTERS: Regex = Regex::new(r#"[\x00-\x1f\x80-\x9f]"#).unwrap();
    pub static ref REGEX_CONTENT_DISPOSITION_FILENAME:Regex = Regex::new(r#"filename="?([^"]*?)"?(;|$)"#).unwrap();
}
const INVALID_CHARACTER_REPLACEMENT:&str = "";
const MAX_PATH_LENGTH:usize=255;
pub const ERROR_TEMPLATE_CREATION:&str = "failed to build Progressbar template";

pub fn base64_encode(value:&str)->String{
    encode(value)
}

pub fn read_file(source_file: &str) -> Vec<String> {
    let mut lines_vec = vec![];
    if let Ok(lines) = read_lines(source_file) {
        for line in lines {
            if let Ok(data) = line {
                lines_vec.push(data)
            }
        }
    }
    lines_vec
}

pub fn read_file_with_seperator(source_file: &str, delimiter: &str) -> Vec<(String, String)> {
    let lines = read_file(source_file);
    let mut lines_vec = vec![];

    for line in lines {
        let ss: Vec<&str> = line.split(delimiter).collect();
        lines_vec.push((ss[0].to_owned(), ss[1].to_owned()))
    }
    lines_vec
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn check_file_name_length(file_name:std::borrow::Cow<str>)->String{
    if file_name.len() > 255{
        let mut end:usize = MAX_PATH_LENGTH;
        loop{
            if file_name.is_char_boundary(end){
                break;
            }
            end -= 1;
        }
        return String::from(&file_name[..end])
    }
    String::from(file_name)
}

pub fn make_file_name_valid<S: AsRef<str>>(name: S) -> String {
    let name = name.as_ref();

    let name = REGEX_ILLEGAL_CHARACTERS.replace_all(&name, INVALID_CHARACTER_REPLACEMENT);
    let name = REGEX_CONTROL_CHARACTERS.replace_all(&name, INVALID_CHARACTER_REPLACEMENT);
    let name = REGEX_RESERVED.replace(&name, INVALID_CHARACTER_REPLACEMENT);
    if cfg!(windows){
        let name = REGEX_WINDOWS_RESERVED.replace(&name, INVALID_CHARACTER_REPLACEMENT);
        let name = REGEX_WINDOWS_TRAILING.replace(&name,INVALID_CHARACTER_REPLACEMENT);
        return check_file_name_length(name);
    }
    return check_file_name_length(name);
}


