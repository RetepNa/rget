use anyhow::anyhow;
use clap::{App, Arg};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle, ProgressDrawTarget};
use reqwest::{
    header::{self, HeaderMap},
    Client,
};
use rget::{
    context::{Context, DownloadFileInfo, Sources},
    ERROR_TEMPLATE_CREATION, REGEX_CONTENT_DISPOSITION_FILENAME, base64_encode,
};
use std::{path::Path, fs::File};
use std::io::Read;
use tokio::{
    fs,
    io::AsyncWriteExt,
    sync::mpsc::{self, Receiver, Sender},
    task::JoinHandle,
};

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let matches = App::new("RGET")
        .version("1.1")
        .about("Downloads files from the internet")
        .arg(
            Arg::with_name("url")
                .value_name("URL")
                .required(true)
                .multiple(true)
                .index(1)
                .conflicts_with_all(&["from-file", "delimiter"])
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output")
                .short('o')
                .long("output")
                .value_name("OUTPUT")
                .required(false)
                .requires("url")
                .ignore_case(true)                
        )
        .arg(
            Arg::with_name("proxy")
                .short('p')
                .long("proxy")
                .value_name("PROXY")
                .required(false),
        )
        .arg(
            Arg::with_name("user-agent")
                .short('u')
                .long("user-agent")
                .value_name("USER-AGENT")
                .required(false),
        )
        .arg(
            Arg::with_name("from-file")
                .long("from-file")
                .value_name("FROM-FILE")
                .conflicts_with_all(&["url"])
                .required(false)
                .help("Load URLs from file, in combination with 'delimiter' the output can be set too.")
        )
        .arg(
            Arg::with_name("delimiter")
                .long("delimiter")
                .value_name("DELIMITER")
                .required(false)
                .requires("from-file")
                .help("In combination with 'from-file'. Splits the line into two pieces. [url]<delimeter>[filename]")
        )
        .arg(
            Arg::with_name("tasks")
                .short('t')
                .long("tasks")
                .default_value("8")
                .value_name("TASKS"),
        )
        .arg(
            Arg::with_name("force")
                .short('f')
                .long("force")
                .value_name("FORCE")
                .takes_value(false)
                .help("If set, rget will overwrite the output")
            )        
        .arg(
            Arg::with_name("auto-file-name")
                .long("auto-file-name")
                .value_name("AUTO-FILE-NAME")
                .env("RGET_AUTO_FILE_NAME")                
                .action(clap::ArgAction::SetTrue)
                .help("If set, rget will try to determinate the filename via the CONTENT_DISPOSITION HTTP Header")
        )
        .arg(
            Arg::with_name("no-progress-bar")
                .long("no-progress-bar")
                .value_name("NO-PROGRESS-BAR")
                .env("RGET_NO_PROGRESS_BAR")
                .action(clap::ArgAction::SetTrue)
                .help("Dont show a progress bar")
        )
        .arg(
            Arg::with_name("authentication-type")
                .long("authentication-type")
                .value_parser(["none","basic"])
                .default_value("none")
                .help("")
        )
        .arg(
            Arg::with_name("username")
                .long("username")
                .required_if_eq("authentication-type", "basic")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("password")
            .long("password")
            .required_if_eq("authentication-type","basic")            
            .takes_value(true)
        )
        .arg(
            Arg::with_name("certificate-path")
                .long("certificate-path")
                .required_if_eq("authentication-type","cert")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("certificate-password")
                .long("certificate-password")
                .required_if_eq("authentication-type","cert")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("token")
                .long("token")
                .required_if_eq("authentication-type", "bearer")
                .takes_value(true)
        )
        .get_matches();

    let client_builder = if matches.is_present("proxy") {
        let proxy = matches.value_of("proxy").unwrap();        
        if proxy == "system" {
            Client::builder()            
        } else {
            Client::builder().proxy(reqwest::Proxy::all(proxy)?)
        }
    } else {
        Client::builder().no_proxy()
    };

    //TODO: Find a better way to handle stuff like this....
    let client_builder = if matches.is_present("user-agent") {
        client_builder.user_agent(matches.value_of("user-agent").unwrap())
    } else {
        client_builder
    };
    
    let authentication = matches.get_one::<String>("authentication-type").unwrap();
    let client_builder = match authentication.as_ref() {
        "cert"=>{
            // TODO
            // Mockup implementation, couse i cant test it now!
            let path = matches.get_one::<String>("certificate-path").unwrap();            
            let mut buf = Vec::new();
            File::open(path).expect("Couldnt read file").read_to_end(&mut buf)?;
            let pkcs12 = reqwest::Identity::from_pkcs12_der(&buf,"")?;
            client_builder.identity(pkcs12)            
        }
        "basic"=>{
            let username = matches.get_one::<String>("username").unwrap();
            let password = matches.get_one::<String>("password").unwrap();
            let mut headers = header::HeaderMap::new();            
            let auth_header = header::HeaderValue::from_str(&format!("Basic {}",base64_encode(&format!("{}:{}",username,password)))).unwrap();
            headers.insert(header::AUTHORIZATION,auth_header);
            client_builder.default_headers(headers)
        }
        "bearer"=>{
            let token = matches.get_one::<String>("token").unwrap();
            let mut headers = header::HeaderMap::new();
            let auth_header = header::HeaderValue::from_str(&format!("Bearer {}",token)).unwrap();
            headers.insert(header::AUTHORIZATION,auth_header);
            client_builder.default_headers(headers)
        }
        _=>{client_builder}
    };

    let client = client_builder.build()?;
    let auto_file_name:bool = *matches.get_one("auto-file-name").unwrap();
    let url_source = if matches.is_present("from-file") {
        let file_path = matches.value_of("from-file").unwrap();
        if matches.is_present("delimiter") {
            Sources::new_with_output(rget::read_file_with_seperator(
                file_path,
                matches.value_of("delimiter").unwrap(),                
            ),auto_file_name)
        } else {
            Sources::new(rget::read_file(file_path),auto_file_name)
        }
    } else {
        let urls: Vec<String> = matches
            .values_of("url")
            .unwrap()
            .map(|x| x.to_owned())
            .collect();
        if matches.is_present("output") {
            let mut download = vec![];
            download.push((
                urls[0].to_owned(),
                matches.value_of("output").unwrap().to_owned(),
            ));
            Sources::new_with_output(download,auto_file_name)
        } else {
            Sources::new(urls,auto_file_name)
        }
    };
    let overwrite_files = matches.is_present("force");
    let max_length = if let Ok(tasks) = matches.value_of("tasks").unwrap().parse::<usize>() {
        if tasks > url_source.len() {
            url_source.len()
        } else {
            tasks
        }
    } else {
        // Failed to parse tasks....
        // so for now we just give one :D
        1
    };
    if max_length == 0 || url_source.len() == 0 {
        eprintln!("Couldn't find any URLs to download!");
        panic!("Sources cant be empty");
    }

    let (tx, mut rx): (Sender<u8>, Receiver<u8>) = mpsc::channel(max_length);

    let context = Context::new_with_source(client, url_source);
    let no_progress_bar:bool = *matches.get_one("no-progress-bar").unwrap();
    
    if no_progress_bar{
        context.multi_bar.set_draw_target(ProgressDrawTarget::hidden());
    }

    let tasks: Vec<JoinHandle<()>> = std::iter::repeat_with(|| {
        let ctx = context.clone();
        ctx.tick();
        let sender = tx.clone();
        tokio::spawn(async move {
            let mut url = ctx.next().await.unwrap();
            let result = download(&ctx.client, &mut url, &ctx.multi_bar, overwrite_files).await;
            sender.send(1).await.expect("Failed to finish");
            ctx.increment_by(1);

            if let Err(err) = result {
                //Maybe handle the error in a differnt way?
                // eprintln!("Download error: {}",err);
                if let Err(print_err) =
                    ctx.multi_bar
                        .println(format!("{} into {}", err, url.file_name()))
                {
                    eprintln!("{} and printing error: {}", err, print_err);
                }
            }
        })
    })
    .take(max_length as usize)
    .collect();

    let finshed_context = context.clone();
    let finished = tokio::spawn(async move {
        for _ in 0..max_length {
            rx.recv().await.unwrap();
        }
        finshed_context.finish();
    });    
    finished.await?;
    futures::future::join_all(tasks).await;

    Ok(())
}
async fn get_header(client: &Client, url: &str) -> Result<HeaderMap, anyhow::Error> {
    let resp = client.head(url).send().await?;
    if resp.status().is_success() {
        let headers = resp.headers();
        return Ok(headers.clone());
    }
    return Err(anyhow!(
        "Failed to download URL: {}, because of Error {:?}",
        url,
        resp.status()
    ));
}
fn get_size(headers: &HeaderMap) -> u64 {
    let total_size = {
        headers
            .get(header::CONTENT_LENGTH)
            .and_then(|x| x.to_str().ok())
            .and_then(|x| x.parse().ok())
            .unwrap_or(0)
    };
    return total_size;
}

fn get_filename_from_header(headers: &HeaderMap) ->Option<&str>{
    if headers.contains_key(header::CONTENT_DISPOSITION) {
        if let Some(content) = headers
            .get(header::CONTENT_DISPOSITION)
            .and_then(|x| x.to_str().ok())
        {
            if let Some(filename_capture) = REGEX_CONTENT_DISPOSITION_FILENAME.captures(content) {
                if filename_capture.len() > 1{                    
                    return Some(filename_capture.get(1).unwrap().as_str());
                }                
            }
        }
    }
    return None;
}

async fn download(
    client: &Client,
    file_info: &mut DownloadFileInfo,
    multi: &MultiProgress,
    overwrite_files: bool,
) -> Result<(), anyhow::Error> {
    let url = &file_info.url();
    let headers = get_header(client, url).await?;
    let total_size = get_size(&headers);
    if file_info.auto_file_name(){
        if let Some(header_file_name) = get_filename_from_header(&headers){
            file_info.set_file_name(header_file_name);     
        }
    }
    let request = client.get(url);
    let bar = multi.add(build_procress_bar(total_size));

    bar.set_message(format!("{}", file_info.file_name()));
    let file = Path::new(file_info.file_name());
    if file.exists() && !overwrite_files {
        bar.set_style(
            ProgressStyle::default_bar()
                .template("{msg}")
                .expect(ERROR_TEMPLATE_CREATION),
        );
        bar.tick();
        bar.abandon_with_message(format!("{} already exists!", file_info.file_name()));
        return Err(anyhow!(format!(
            "{} already exists!",
            file_info.file_name()
        )));
    }

    let download_file = Path::new(file_info.temp_file_name());
    let mut dest = if download_file.exists() {
        fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&download_file)
            .await?
    } else {
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&download_file)
            .await?
    };

    let mut source = request.send().await?;

    while let Some(data) = source.chunk().await? {
        dest.write_all(&data).await?;
        bar.inc(data.len() as u64);
    }

    fs::rename(file_info.temp_file_name(), file_info.file_name()).await?;
    bar.finish();
    Ok(())
}

fn build_procress_bar(size: u64) -> ProgressBar {
    let bar = ProgressBar::new(size);
    bar.set_style(ProgressStyle::default_bar()
                    .template("{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {bytes}/{total_bytes} ({eta}) {msg}").expect(ERROR_TEMPLATE_CREATION)
                    .progress_chars("#>-"));

    return bar;
}
