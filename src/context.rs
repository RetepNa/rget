use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use reqwest::Client;
use std::{
    ffi::OsStr,
    path::Path,
    sync::{Arc, Mutex},
};

use crate::{make_file_name_valid, ERROR_TEMPLATE_CREATION};

#[derive(Clone)]
pub struct Context {
    pub client: Client,
    sources: Arc<Mutex<Sources>>,
    pub multi_bar: Arc<MultiProgress>,
    main_bar: Arc<Mutex<ProgressBar>>,
}

impl Context {
    pub fn new(client: Client, urls: Vec<String>) -> Context {
        let mut context = Context {
            client: client,
            sources: Arc::new(Mutex::new(Sources::new(urls,false))),
            multi_bar: Arc::new(MultiProgress::new()),
            main_bar: Arc::new(Mutex::new(ProgressBar::new(0))),
        };
        context.build_main_bar();
        context
    }

    pub fn new_with_output(client: Client, urls: Vec<(String, String)>) -> Context {
        let mut context = Context {
            client: client,
            sources: Arc::new(Mutex::new(Sources::new_with_output(urls,false))),
            multi_bar: Arc::new(MultiProgress::new()),
            main_bar: Arc::new(Mutex::new(ProgressBar::new(0))),
        };
        context.build_main_bar();
        context
    }

    pub fn new_with_source(client:Client,sources:Sources)->Context{
        let mut context = Context {
            client: client,
            sources: Arc::new(Mutex::new(sources)),
            multi_bar: Arc::new(MultiProgress::new()),
            main_bar: Arc::new(Mutex::new(ProgressBar::new(0))),
        };
        context.build_main_bar();
        context
    }

    fn build_main_bar(&mut self) {
        let bar = self
            .multi_bar
            .add(ProgressBar::new(self.sources_len() as u64));
        bar.set_style(
            ProgressStyle::default_bar().template("{bar:40.green/yellow} {pos:>4}/{len:4}").expect(ERROR_TEMPLATE_CREATION),
        );
        self.main_bar = Arc::new(Mutex::new(bar));
    }

    pub async fn next(&self) -> Option<DownloadFileInfo> {
        let mut store = self.sources.lock().unwrap();
        return store.urls.pop();
    }
    pub fn tick(&self) {
        self.main_bar.lock().unwrap().tick();
    }

    pub fn finish(&self) {
        self.main_bar.lock().unwrap().finish();
    }
    pub fn increment_by(&self, i: u64) {
        self.main_bar.lock().unwrap().inc(i);
        self.tick();
    }
    pub fn sources_len(&self) -> usize {
        self.sources.lock().unwrap().len()
    }
}

#[derive(Debug, Default)]
pub struct Sources {
    urls: Vec<DownloadFileInfo>,
}

impl Sources {
    pub fn new(urls: Vec<String>,auto_filename:bool) -> Sources {
        let mut tmp = vec![];

        for i in urls {
            tmp.push(DownloadFileInfo::new(i,auto_filename))
        }
        Sources { urls: tmp }
    }

    pub fn new_with_output(urls: Vec<(String, String)>,auto_filename:bool) -> Sources {
        let mut tmp = vec![];

        for i in urls {
            tmp.push(DownloadFileInfo::new_with_output(i.0, i.1,auto_filename));
        }

        Sources { urls: tmp }
    }

    pub fn len(&self) -> usize {
        self.urls.len()
    }
}

#[derive(Debug, Default, Clone)]
pub struct DownloadFileInfo {
    url: String,
    filename: String,
    tmpname: String,
    auto_file_name:bool
}

impl DownloadFileInfo {
    pub fn new(url: String,auto_file_name:bool) -> DownloadFileInfo {        
        let filename = Path::file_name(Path::new(&url)).unwrap_or(&OsStr::new("tmp.bin"));
        let mut filename_str = filename.to_str().unwrap().to_string();
        filename_str = make_file_name_valid(&filename_str);        
        let download_filename_str = format!("{}~~", filename_str);       
        DownloadFileInfo {
            filename: filename_str,
            tmpname: download_filename_str,
            url,
            auto_file_name
        }
    }
    pub fn new_with_output(url: String, filename: String,auto_file_name:bool) -> DownloadFileInfo {
        let download_filename_str = format!("{}~~", filename);
        DownloadFileInfo {
            filename,
            tmpname: download_filename_str,
            url,
            auto_file_name
        }
    }
    pub fn auto_file_name(&self)->bool{
        self.auto_file_name
    }
    pub fn file_name(&self) -> &str {
        &self.filename
    }
    pub fn temp_file_name(&self) -> &str {
        &self.tmpname
    }
    pub fn url(&self) -> String {
        self.url.clone()
    }
    pub fn set_file_name(&mut self,name:&str){
        self.filename = name.into();
        self.tmpname = format!("{}~~", name);
    }
}
